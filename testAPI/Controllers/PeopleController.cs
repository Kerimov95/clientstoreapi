﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using testAPI.Models;

namespace testAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PeopleController : ControllerBase
    {
        [HttpGet]
        public List<Hero> heroes()
        {
            return DataBase.heros;
        }

        [HttpPost]
        public Hero AddHero([FromBody] Hero hero)
        {
            DataBase.heros.Add(hero);
            return hero;
        }

        [HttpPut]
        public Hero editHero([FromBody] Hero hero)
        {
            var currentHero = DataBase.heros.FirstOrDefault(h => h.id == hero.id);

            currentHero = hero;

            return hero;
        }

        [HttpDelete]
        public void deleteHero([FromQuery] int id)
        {
            var delRow = DataBase.heros.FirstOrDefault(r => r.id == id);
            DataBase.heros.Remove(delRow);
        }

    }
}
