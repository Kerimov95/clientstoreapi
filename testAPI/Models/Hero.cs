﻿using System;


namespace testAPI.Models
{
    public class Hero
    {
        public int id { set; get; }
        public string name { set; get; }
        public int age { set; get; }
    }
}
